"""
TODO: Module Docstring
Create methods that separate crime data

1. Create a method that separates each line of data (create a dict with Record ID as key, list as value) and cleans

2. Create a method to separate data by zip code and return max crime types by zip code


"""

import re
from itertools import groupby
from geopy import distance
from geopy.geocoders import Nominatim, GoogleV3



class Crime:
    '''
    Portland Crime Data Model
    '''
    pass

    def __init__(self, record_id, report_date, report_time,
                 major_offense_type, address, neighborhood,
                 police_precinct, police_district, point):

        self.record_id = record_id
        self.report_date = report_date
        self.report_time = report_time
        self.major_offense_type = major_offense_type
        self.address = address # Contains city, state, and zip.
        #self.city =
        #self.state =
        #self.zipcode =
        #self.extract_zipcode() # Fix for PDX Dataset commas in addresses.
        self.neighborhood = neighborhood
        self.police_precinct = police_precinct
        self.police_district = police_district
        self.point = point # Contains lat,long
        self.x_coord = None
        self.y_coord = None
        self.geocoder = None

    def __str__(self):
        return "Crime record {0.record_id}, Major Offense Type: {0.major_offense_type}, \
                ZIP Code: {0.zip_code}".format(self)

    def __repr__(self):
        return "{0.__class__.__name__}({0.record_id}, {0.report_date}, {0.report_time}, {0.major_offense_type}, {0.address}, \
               {0.neighborhood}, {0.police_precinct}, {0.police_district}, {0.point})".format(self)

    def geocode(self):
        '''
        Takes the address string(?), cleans address data for geopy instance storing them respectively on self.
        Use geopy's Nominatim Class.
        Also see this for making point objects.
        https://geopy.readthedocs.io/en/1.10.0/#geopy.point.Point
        https://geopy.readthedocs.io/en/1.10.0/#geopy.point.Point.from_sequence
        self.address = self.address.strip().replace('block of', '')
        geocoder = Nominatim()
        self.geocoder = geocoder.geocode(self.address)
        self.point = self.geocoder.point
        return self.geocoder
        '''
        pass


    @classmethod
    def load_json(Klass):
        '''
        Returns one crime object for a given JSON representation.
        '''
        pass

    def radius(self, point):
        '''
        Returns all crimes in a radius.
        '''
        pass

    def distance(self, point):
        '''
        returns the crimes distance to another point.
        '''
        pass


class CrimeData:
    pass

    def __init__(self, csv, *args, **kwargs):
        self.filename = csv
        self.data = None
        self.crimes = None # this is where the crime object list is stored after cleaning
        self.read()
        self.make_crimes()

    def __str__(self):
        return "List of crime data from {0.filename}".format(self)

    def __repr__(self):
        '''
        #TODO: __repr__
        '''
        return "{0.__class__.__name__}('{0.filename}')".format(self)


    def status(self):
        '''
        Returns readable status for sanity.
        '''
        pass

    @property
    def count(self):
        '''
        returns the number of crime objects stored.
        '''
        return len(self.crimes)

    @staticmethod
    def validate_crime(crime_details):
        '''
        Pass through for debugging - TODO: Perform some validation here.
        '''
        # Checkfor 9 args from the parsed file input logic.
        if len(crime_details) != 9:
            import pdb; pdb.set_trace()
        else:
            return crime_details

    def clean_line(self, line):
        '''
        Cleaning up lines, one crime at a time...
        Helper function for crimdata.read()
        Cleans one line of text, returns a list of crime detail for one crime.
        '''
        line = line.replace('\n', '').lower()
        crime_details = [cell.strip().replace('\"', '') for cell in line.split('\",')]
        #import pdb; pdb.set_trace()


        validated = self.validate_crime(crime_details)
        return validated


    def read(self):
        """
        opens and cleans .csv file, adds each line to a list, and removes quotations.
        """
        with open(self.filename, 'r', encoding='utf-8') as file:
            file = file.readlines()
            header = file.pop(0) # Remove the Header Line, Unused var at this time.
            data = [self.clean_line(line) for line in file]

            self.data = data
        return self.data

    def make_crimes(self):
        '''
        Creates crimes from comma parsed crime record lists.
        '''
        crimes = list()
        for record in self.data:
            crime = Crime(*record) # Passes the whole list as a tuple to the Crime constructor for unpacking.
            crimes.append(crime)

        self.crimes = crimes
        return self.crimes

    def offense_freq(self):
        '''
        Produces a list of dictionary of offense type frequency.
        '''

        crime_lib = sorted(self.crimes, key=lambda c: c.major_offense_type)
        crime_groups = groupby(crime_lib, lambda c: c.major_offense_type)
        '''
            crime_frequency_dict = dict()
                for major_offense_type, crimes in crime_groups:
            crime_frequency_dict[major_offense_type] = len(list(crimes))
        '''
        crime_frequency_dict = {k: len(list(v)) for k, v in crime_groups}

        return crime_frequency_dict
        #pass

    def max_crimes(self, key):
        '''
        Returns a maxium value based upon a key.  ie: max crimes by ______.
        '''
        pass

    def daily_record(self, date=None):
        '''
        Returns all crimes for a given day
        '''
        pass

    def daterange(self, start, stop):
        '''
        Returns crimes over a daterange.
        '''
        pass

    def to_json(self, file=None):
        '''
        Returns JSON for each crime object. Optionally writes to a file.
        '''
        pass

    def kwsearch(self, keyword):
        '''
        searches crimes by keyword(s).
        Returns a list of crimes.
        '''
        pass

    def crimes_from_zip(self, zipcode):
        '''
        Returns a list of crimes for a given zipcode.
        '''
        pass

    def datastats(self):
        '''
        An Overview of the data.
        '''
        pass
