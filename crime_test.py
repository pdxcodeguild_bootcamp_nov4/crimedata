import unittest
from crime import Crime
from crime import CrimeData
from decorators import expected_failure
#from geopy import distance
#from geopy.geocoders import Nominatim


class TestCrimeData(unittest.TestCase):
    def setUp(self):
        # Makes a test crime instance.
        self.test_crime = Crime('12345678', '01/01/2014', '12:00:00', 'disorderly conduct', '100-398 block of ne russell st, portland, or 97212', 'eliot', 'portland prec no', '590', '7647676.71129,690734.35597000003')

        #Instantiating CrimeData for each avaialble CSV
        self.test_crime_data_2012 = CrimeData("data/crime_incident_data_2012.csv")
        self.test_crime_data_2013 = CrimeData("data/crime_incident_data_2013.csv")
        self.test_crime_data_2014 = CrimeData("data/crime_incident_data_2014.csv")
        self.test_crime_data_recent = CrimeData("data/crime_incident_data_recent.csv")

    def test_offense_freq_accuracy(self):
        self.assertEqual(self.test_crime_data_2014.offense_freq(),
        {'weapons': 240, 'curfew': 13, 'burglary': 3259, 'rape': 156,
        'robbery': 668, 'disorderly conduct': 2960, 'duii': 1115,
        'sex offenses': 328, 'drugs': 1842, 'assault, simple': 3064,
        'vandalism': 3403, 'offenses against family': 31,
        'aggravated assault': 1281, 'larceny': 18802, 'kidnap': 17,
        'forgery': 462, 'prostitution': 120, 'fraud': 1445,
        'homicide': 21, 'runaway': 1053, 'gambling': 1,
        'stolen property': 62, 'trespass': 2115, 'arson': 124,
        'motor vehicle theft': 2937, 'liquor laws': 1640,
        'embezzlement': 111})

    def tearDown(self):
        # Removing references to our crimedata objects.
        del self.test_crime_data_2012
        del self.test_crime_data_2013
        del self.test_crime_data_2014

    def test_crime_count(self):
        # Tests counts on crimedata records for each year.
        self.assertEqual(self.test_crime_data_2012.count, 63279)
        self.assertEqual(self.test_crime_data_2013.count, 61779)
        self.assertEqual(self.test_crime_data_2014.count, 47270)

class TestGeoCoder(TestCrimeData):
    def setUp(self):
        # Makes a test crime instance.
        self.test_crime = Crime(record_id='12345678',
                                report_date='01/01/2014',
                                report_time='12:00:00',
                                major_offense_type='disorderly conduct',
                                address='100-398 block of ne russell st, portland, or 97212',
                                neighborhood='eliot',
                                police_precinct='portland prec no',
                                police_district='590',
                                point='7647676.71129,690734.35597000003')

        #Instantiating CrimeData for each avaialble CSV
        self.test_crime_data_2012 = CrimeData("data/crime_incident_data_2012.csv")
        self.test_crime_data_2013 = CrimeData("data/crime_incident_data_2013.csv")
        self.test_crime_data_2014 = CrimeData("data/crime_incident_data_2014.csv")
        self.test_crime_data_recent = CrimeData("data/crime_incident_data_recent.csv")

    def tearDown(self):
        # Removing references to our crimedata objects.
        del self.test_crime_data_2012
        del self.test_crime_data_2013
        del self.test_crime_data_2014
        del self.test_crime_data_recent

    @expected_failure
    def test_crime_geocode_(self):
        self.assertEqual(self.test_crime_data_2013.crimes[1], None)
