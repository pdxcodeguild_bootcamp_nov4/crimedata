import functools
import nose


def expected_failure(test):
    @functools.wraps(test)
    def wrapped_test(*args, **kwargs):
        try:
            test(*args, **kwargs)
        except Exception:
            raise nose.SkipTest
        else:
            raise AssertionError('Failure expected')
    return wrapped_test
